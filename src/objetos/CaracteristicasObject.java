package objetos;


import java.util.ArrayList;

import com.google.gson.Gson;

import classificadores.Classificador;


public class CaracteristicasObject {
	private String arquivo;
	private double[] areaMethodofMomentsofMFCCsOverallStandardDeviation;
	private double[] areaMethodofMomentsofMFCCsOverallAverage;
	private double spectralCentroidOverallStandardDeviation;
	private double spectralRolloffPointOverallStandardDeviation;
	private double spectralFluxOverallStandardDeviation;
	private double compactnessOverallStandardDeviation;
	private double spectralVariabilityOverallStandardDeviation;
	private double rootMeanSquareOverallStandardDeviation;
	private double fractionOfLowEnergyWindowsOverallStandardDeviation;
	private double zeroCrossingsOverallStandardDeviation;
	private double strongestBeatOverallStandardDeviation;
	private double beatSumOverallStandardDeviation;
	private double strengthOfStrongestBeatOverallStandardDeviation;
	private double[] mFCCOverallStandardDeviation;
	private double[] lPCOverallStandardDeviation;
	private double[] methodofMomentsOverallStandardDeviation;
	private double spectralCentroidOverallAverage;
	private double spectralRolloffPointOverallAverage;
	private double spectralFluxOverallAverage;
	private double compactnessOverallAverage;
	private double spectralVariabilityOverallAverage;
	private double rootMeanSquareOverallAverage;
	private double fractionOfLowEnergyWindowsOverallAverage;
	private double zeroCrossingsOverallAverage;
	private double strongestBeatOverallAverage;
	private double beatSumOverallAverage;
	private double strengthOfStrongestBeatOverallAverage;
	private double[] mFCCOverallAverage;
	private double[] lPCOverallAverage;
	private double[] methodofMomentsOverallAverage;
	private String grupo;
	private String previsao;
	
	private String[] caracteristicasNome = {"AreaMethodofMomentsofMFCCsOverallStandardDeviation",
	"AreaMethodofMomentsofMFCCsOverallAverage",
	"spectralCentroidOverallStandardDeviation",
	"SpectralRolloffPointOverallStandardDeviation",
	"SpectralFluxOverallStandardDeviation",
	"CompactnessOverallStandardDeviation",
	"SpectralVariabilityOverallStandardDeviation",
	"RootMeanSquareOverallStandardDeviation",
	"FractionOfLowEnergyWindowsOverallStandardDeviation",
	"ZeroCrossingsOverallStandardDeviation",
	"StrongestBeatOverallStandardDeviation",
	"BeatSumOverallStandardDeviation",
	"StrengthOfStrongestBeatOverallStandardDeviation",
	"LPCOverallStandardDeviation",
	"MethodofMomentsOverallStandardDeviation",
	"SpectralCentroidOverallAverage",
	"SpectralRolloffPointOverallAverage",
	"SpectralFluxOverallAverage",
	"CompactnessOverallAverage",
	"SpectralVariabilityOverallAverage",
	"RootMeanSquareOverallAverage",
	"FractionOfLowEnergyWindowsOverallAverage",
	"ZeroCrossingsOverallAverage",
	"StrongestBeatOverallAverage",
	"BeatSumOverallAverage",
	"StrengthOfStrongestBeatOverallAverage",
	"LPCOverallAverage",
	"MethodofMomentsOverallAverage"};


	public CaracteristicasObject(String arquivo,
			double spectralCentroidOverallStandardDeviation,
			double spectralRolloffPointOverallStandardDeviation, 
			double spectralFluxOverallStandardDeviation,
			double compactnessOverallStandardDeviation, 
			double spectralVariabilityOverallStandardDeviation,
			double rootMeanSquareOverallStandardDeviation, 
			double fractionOfLowEnergyWindowsOverallStandardDeviation,
			double zeroCrossingsOverallStandardDeviation, 
			double strongestBeatOverallStandardDeviation,
			double beatSumOverallStandardDeviation, 
			double strengthOfStrongestBeatOverallStandardDeviation,
			double[] lPCOverallStandardDeviation, 
			double[] methodofMomentsOverallStandardDeviation,
			double[] areaMethodofMomentsofMFCCsOverallStandardDeviation,
			double spectralCentroidOverallAverage, 
			double spectralRolloffPointOverallAverage,
			double spectralFluxOverallAverage, 
			double compactnessOverallAverage,
			double spectralVariabilityOverallAverage, 
			double rootMeanSquareOverallAverage,
			double fractionOfLowEnergyWindowsOverallAverage, 
			double zeroCrossingsOverallAverage,
			double strongestBeatOverallAverage, 
			double beatSumOverallAverage,
			double strengthOfStrongestBeatOverallAverage, 
			double[] lPCOverallAverage,
			double[] methodofMomentsOverallAverage, 
			double[] areaMethodofMomentsofMFCCsOverallAverage, 
			String grupo) {
		super();
		this.arquivo = arquivo;
		this.areaMethodofMomentsofMFCCsOverallStandardDeviation = areaMethodofMomentsofMFCCsOverallStandardDeviation;
		this.areaMethodofMomentsofMFCCsOverallAverage = areaMethodofMomentsofMFCCsOverallAverage;
		this.spectralCentroidOverallStandardDeviation = spectralCentroidOverallStandardDeviation;
		this.spectralRolloffPointOverallStandardDeviation = spectralRolloffPointOverallStandardDeviation;
		this.spectralFluxOverallStandardDeviation = spectralFluxOverallStandardDeviation;
		this.compactnessOverallStandardDeviation = compactnessOverallStandardDeviation;
		this.spectralVariabilityOverallStandardDeviation = spectralVariabilityOverallStandardDeviation;
		this.rootMeanSquareOverallStandardDeviation = rootMeanSquareOverallStandardDeviation;
		this.fractionOfLowEnergyWindowsOverallStandardDeviation = fractionOfLowEnergyWindowsOverallStandardDeviation;
		this.zeroCrossingsOverallStandardDeviation = zeroCrossingsOverallStandardDeviation;
		this.strongestBeatOverallStandardDeviation = strongestBeatOverallStandardDeviation;
		this.beatSumOverallStandardDeviation = beatSumOverallStandardDeviation;
		this.strengthOfStrongestBeatOverallStandardDeviation = strengthOfStrongestBeatOverallStandardDeviation;
		this.lPCOverallStandardDeviation = lPCOverallStandardDeviation;
		this.methodofMomentsOverallStandardDeviation = methodofMomentsOverallStandardDeviation;
		this.spectralCentroidOverallAverage = spectralCentroidOverallAverage;
		this.spectralRolloffPointOverallAverage = spectralRolloffPointOverallAverage;
		this.spectralFluxOverallAverage = spectralFluxOverallAverage;
		this.compactnessOverallAverage = compactnessOverallAverage;
		this.spectralVariabilityOverallAverage = spectralVariabilityOverallAverage;
		this.rootMeanSquareOverallAverage = rootMeanSquareOverallAverage;
		this.fractionOfLowEnergyWindowsOverallAverage = fractionOfLowEnergyWindowsOverallAverage;
		this.zeroCrossingsOverallAverage = zeroCrossingsOverallAverage;
		this.strongestBeatOverallAverage = strongestBeatOverallAverage;
		this.beatSumOverallAverage = beatSumOverallAverage;
		this.strengthOfStrongestBeatOverallAverage = strengthOfStrongestBeatOverallAverage;
		this.lPCOverallAverage = lPCOverallAverage;
		this.methodofMomentsOverallAverage = methodofMomentsOverallAverage;
		this.grupo = grupo;
	}

	public CaracteristicasObject(String classe,ArrayList<double[]> caracteristicas) {
		super();
		grupo = classe;
		spectralCentroidOverallStandardDeviation = caracteristicas.get(0)[0];
		spectralRolloffPointOverallStandardDeviation = caracteristicas.get(1)[0];
		spectralFluxOverallStandardDeviation = caracteristicas.get(2)[0];
		compactnessOverallStandardDeviation = caracteristicas.get(3)[0];
		spectralVariabilityOverallStandardDeviation = caracteristicas.get(4)[0];
		rootMeanSquareOverallStandardDeviation = caracteristicas.get(5)[0];
		fractionOfLowEnergyWindowsOverallStandardDeviation = caracteristicas.get(6)[0];
		zeroCrossingsOverallStandardDeviation = caracteristicas.get(7)[0];
		strongestBeatOverallStandardDeviation = caracteristicas.get(8)[0];
		beatSumOverallStandardDeviation = caracteristicas.get(9)[0];
		strengthOfStrongestBeatOverallStandardDeviation = caracteristicas.get(10)[0];
		mFCCOverallStandardDeviation = caracteristicas.get(11);
		lPCOverallStandardDeviation = caracteristicas.get(12);
		methodofMomentsOverallStandardDeviation = caracteristicas.get(13);
		areaMethodofMomentsofMFCCsOverallStandardDeviation = caracteristicas.get(14);
		spectralCentroidOverallAverage = caracteristicas.get(15)[0];
		spectralRolloffPointOverallAverage = caracteristicas.get(16)[0];
		spectralFluxOverallAverage = caracteristicas.get(17)[0];
		compactnessOverallAverage = caracteristicas.get(18)[0];
		spectralVariabilityOverallAverage = caracteristicas.get(19)[0];
		rootMeanSquareOverallAverage = caracteristicas.get(20)[0];
		fractionOfLowEnergyWindowsOverallAverage = caracteristicas.get(21)[0];
		zeroCrossingsOverallAverage = caracteristicas.get(22)[0];
		strongestBeatOverallAverage = caracteristicas.get(23)[0];
		beatSumOverallAverage = caracteristicas.get(24)[0];
		strengthOfStrongestBeatOverallAverage = caracteristicas.get(25)[0];
		mFCCOverallAverage = caracteristicas.get(26);
		lPCOverallAverage = caracteristicas.get(27);
		methodofMomentsOverallAverage = caracteristicas.get(28);
		areaMethodofMomentsofMFCCsOverallAverage = caracteristicas.get(29);
		
//		Spectral Centroid Overall Standard Deviation
//		Spectral Rolloff Point Overall Standard Deviation
//		Spectral Flux Overall Standard Deviation
//		Compactness Overall Standard Deviation
//		Spectral Variability Overall Standard Deviation
//		Root Mean Square Overall Standard Deviation
//		Fraction Of Low Energy Windows Overall Standard Deviation
//		Zero Crossings Overall Standard Deviation
//		Strongest Beat Overall Standard Deviation
//		Beat Sum Overall Standard Deviation
//		Strength Of Strongest Beat Overall Standard Deviation
//		MFCC Overall Standard Deviation
//		LPC Overall Standard Deviation
//		Method of Moments Overall Standard Deviation
//		Area Method of Moments of MFCCs Overall Standard Deviation
//		Spectral Centroid Overall Average
//		Spectral Rolloff Point Overall Average
//		Spectral Flux Overall Average
//		Compactness Overall Average
//		Spectral Variability Overall Average
//		Root Mean Square Overall Average
//		Fraction Of Low Energy Windows Overall Average
//		Zero Crossings Overall Average
//		Strongest Beat Overall Average
//		Beat Sum Overall Average
//		Strength Of Strongest Beat Overall Average
//		MFCC Overall Average
//		LPC Overall Average
//		Method of Moments Overall Average
//		Area Method of Moments of MFCCs Overall Average
	}
	public String getArquivo() {
		return arquivo;
	}

	public void setArquivo(String arquivo) {
		this.arquivo = arquivo;
	}


	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getPrevisao() {
		return previsao;
	}

	public void setPrevisao(String previsao) {
		this.previsao = previsao;
	}

	@Override

	public String toString() {
		// TODO Auto-generated method stub
		return new Gson().toJson(this);
	}

	public double compare(CaracteristicasObject comparador, int indexCaracteristica) {
		double diferenca = 0;

		switch (indexCaracteristica) {
		
		case SPECTRAL_CENTROID_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.spectralCentroidOverallStandardDeviation - comparador.getSpectralCentroidOverallStandardDeviation());
			break;
		
		case SPECTRAL_ROLLOFF_POINT_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.spectralRolloffPointOverallStandardDeviation - comparador.getSpectralRolloffPointOverallStandardDeviation());
			break;
		
		case SPECTRAL_FLUX_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.spectralFluxOverallStandardDeviation	- comparador.getSpectralFluxOverallStandardDeviation());
			break;
		
		case COMPACTNESS_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.compactnessOverallStandardDeviation - comparador.getCompactnessOverallStandardDeviation());
			break;
		
		case SPECTRAL_VARIABILITY_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.spectralVariabilityOverallStandardDeviation - comparador.getSpectralVariabilityOverallStandardDeviation());
			break;
		
		case ROOT_MEAN_SQUARE_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.rootMeanSquareOverallStandardDeviation - comparador.getRootMeanSquareOverallStandardDeviation());
			break;
		
		case FRACTION_OF_LOW_ENERGY_WINDOWS_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.fractionOfLowEnergyWindowsOverallStandardDeviation - comparador.getFractionOfLowEnergyWindowsOverallStandardDeviation());
			break;
		
		case ZERO_CROSSINGS_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.zeroCrossingsOverallStandardDeviation - comparador.getZeroCrossingsOverallStandardDeviation());
			break;
		
		case STRONGEST_BEAT_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.strongestBeatOverallStandardDeviation - comparador.getStrongestBeatOverallStandardDeviation());
			break;
		
		case BEAT_SUM_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.beatSumOverallStandardDeviation - comparador.getBeatSumOverallStandardDeviation());
			break;
		
		case STRENGTH_OF_STRONGEST_BEAT_OVERALL_STANDARD_DEVIATION:
				diferenca += Math.abs(this.strengthOfStrongestBeatOverallStandardDeviation - comparador.getStrengthOfStrongestBeatOverallStandardDeviation());
			break;

		case MFCC_OVERALL_STANDARD_DEVIATION:
			for (int i = 0; i < mFCCOverallStandardDeviation.length; i++) {
				diferenca += Math.abs(this.mFCCOverallStandardDeviation[i] - comparador.getmFCCOverallStandardDeviation()[i]);
			}
			break;
		
		case LPC_OVERALL_STANDARD_DEVIATION:
			for (int i = 0; i < lPCOverallStandardDeviation.length; i++) {
				diferenca += Math
						.abs(this.lPCOverallStandardDeviation[i] - comparador.getlPCOverallStandardDeviation()[i]);
			}
			break;
		
		case METHOD_OF_MOMENTS_OVERALL_STANDARD_DEVIATION:
			for (int i = 0; i < methodofMomentsOverallStandardDeviation.length; i++) {
				diferenca += Math.abs(this.methodofMomentsOverallStandardDeviation[i] - comparador.getMethodofMomentsOverallStandardDeviation()[i]);
			}
			break;
		
		case AREA_METHOD_OF_MOMENTS_OF_MFCCS_OVERALL_STANDARD_DEVIATION:
			for (int i = 0; i < areaMethodofMomentsofMFCCsOverallStandardDeviation.length; i++) {
				diferenca += Math.abs(this.areaMethodofMomentsofMFCCsOverallStandardDeviation[i] - comparador.getAreaMethodofMomentsofMFCCsOverallStandardDeviation()[i]);
			}
			break;
		
		case SPECTRAL_CENTROID_OVERALL_AVERAGE:
				diferenca += Math.abs(
						this.spectralCentroidOverallAverage - comparador.getSpectralCentroidOverallAverage());
			break;
		
		case SPECTRAL_ROLL_OFF_POINT_OVERALL_AVERAGE:
				diferenca += Math.abs(this.spectralRolloffPointOverallAverage - comparador.getSpectralRolloffPointOverallAverage());
			break;
		
		case SPECTRAL_FLUX_OVERALL_AVERAGE:
				diferenca += Math.abs(this.spectralFluxOverallAverage - comparador.getSpectralFluxOverallAverage());
			break;
		
		case COMPACTNESS_OVERALL_AVERAGE:
				diferenca += Math.abs(this.compactnessOverallAverage - comparador.getCompactnessOverallAverage());
			break;
		
		case SPECTRALVARIABILITYOVERALLAVERAGE:
				diferenca += Math.abs(this.spectralVariabilityOverallAverage - comparador.getSpectralVariabilityOverallAverage());
			break;
		
		case ROOT_MEAN_SQUARE_OVERALL_AVERAGE:
				diferenca += Math.abs(this.rootMeanSquareOverallAverage - comparador.getRootMeanSquareOverallAverage());
			break;
		
		case FRACTION_OF_LOW_ENERGY_WINDOWS_OVERALL_AVERAGE:
			for (int i = 0; i < areaMethodofMomentsofMFCCsOverallAverage.length; i++) {
				diferenca += Math.abs(this.fractionOfLowEnergyWindowsOverallAverage - comparador.getFractionOfLowEnergyWindowsOverallAverage());
			}
			break;
		
		case ZERO_CROSSINGS_OVERALL_AVERAGE:
				diferenca += Math.abs(this.zeroCrossingsOverallAverage - comparador.getZeroCrossingsOverallAverage());
			break;
		
		case STRONGEST_BEAT_OVERALL_AVERAGE:
				diferenca += Math.abs(this.strongestBeatOverallAverage - comparador.getStrongestBeatOverallAverage());
			break;
		
		case BEAT_SUM_OVERALL_AVERAGE:
				diferenca += Math.abs(this.beatSumOverallAverage - comparador.getBeatSumOverallAverage());
			break;
		
		case STRENGTH_OF_STRONGEST_BEAT_OVERALL_AVERAGE:
				diferenca += Math.abs(this.strengthOfStrongestBeatOverallAverage - comparador.getStrengthOfStrongestBeatOverallAverage());
			break;
		
		case LPC_OVERALL_AVERAGE:
			for (int i = 0; i < lPCOverallAverage.length; i++) {
				diferenca += Math.abs(this.lPCOverallAverage[i] - comparador.getlPCOverallAverage()[i]);
			}
			break;
		
		case METHOD_OF_MOMENTS_OVERALL_AVERAGE:
			for (int i = 0; i < methodofMomentsOverallAverage.length; i++) {
				diferenca += Math.abs(this.methodofMomentsOverallAverage[i] - comparador.getMethodofMomentsOverallAverage()[i]);
			}
			break;
		
		case AREA_METHOD_OF_MOMENTS_OF_MFCCS_OVERALL_AVERAGE:
			for (int i = 0; i < areaMethodofMomentsofMFCCsOverallAverage.length && i < comparador.getAreaMethodofMomentsofMFCCsOverallAverage().length; i++) {
				diferenca += Math.abs(this.areaMethodofMomentsofMFCCsOverallAverage[i] - comparador.getAreaMethodofMomentsofMFCCsOverallAverage()[i]);
			}
			break;

		case MFCC_Overall_Average:
			for (int i = 0; i < mFCCOverallAverage.length && i < comparador.getmFCCOverallAverage().length; i++) {
				diferenca += Math.abs(this.mFCCOverallAverage[i] - comparador.getmFCCOverallAverage()[i]);
			}
			break;

		default:
			break;
		}
		return diferenca;		
	}

	
	public double[] getmFCCOverallAverage() {
		return mFCCOverallAverage;
	}

	public double[] getmFCCOverallStandardDeviation() {
		return mFCCOverallStandardDeviation;
	}

	public double[] getAreaMethodofMomentsofMFCCsOverallStandardDeviation() {
		return areaMethodofMomentsofMFCCsOverallStandardDeviation;
	}

	public double[] getAreaMethodofMomentsofMFCCsOverallAverage() {
		return areaMethodofMomentsofMFCCsOverallAverage;
	}

	public double getSpectralCentroidOverallStandardDeviation() {
		return spectralCentroidOverallStandardDeviation;
	}

	public double getSpectralRolloffPointOverallStandardDeviation() {
		return spectralRolloffPointOverallStandardDeviation;
	}

	public double getSpectralFluxOverallStandardDeviation() {
		return spectralFluxOverallStandardDeviation;
	}

	public double getCompactnessOverallStandardDeviation() {
		return compactnessOverallStandardDeviation;
	}

	public double getSpectralVariabilityOverallStandardDeviation() {
		return spectralVariabilityOverallStandardDeviation;
	}

	public double getRootMeanSquareOverallStandardDeviation() {
		return rootMeanSquareOverallStandardDeviation;
	}

	public double getFractionOfLowEnergyWindowsOverallStandardDeviation() {
		return fractionOfLowEnergyWindowsOverallStandardDeviation;
	}

	public double getZeroCrossingsOverallStandardDeviation() {
		return zeroCrossingsOverallStandardDeviation;
	}

	public double getStrongestBeatOverallStandardDeviation() {
		return strongestBeatOverallStandardDeviation;
	}

	public double getBeatSumOverallStandardDeviation() {
		return beatSumOverallStandardDeviation;
	}

	public double getStrengthOfStrongestBeatOverallStandardDeviation() {
		return strengthOfStrongestBeatOverallStandardDeviation;
	}

	public double[] getlPCOverallStandardDeviation() {
		return lPCOverallStandardDeviation;
	}

	public double[] getMethodofMomentsOverallStandardDeviation() {
		return methodofMomentsOverallStandardDeviation;
	}

	public double getSpectralCentroidOverallAverage() {
		return spectralCentroidOverallAverage;
	}

	public double getSpectralRolloffPointOverallAverage() {
		return spectralRolloffPointOverallAverage;
	}

	public double getSpectralFluxOverallAverage() {
		return spectralFluxOverallAverage;
	}

	public double getCompactnessOverallAverage() {
		return compactnessOverallAverage;
	}

	public double getSpectralVariabilityOverallAverage() {
		return spectralVariabilityOverallAverage;
	}

	public double getRootMeanSquareOverallAverage() {
		return rootMeanSquareOverallAverage;
	}

	public double getFractionOfLowEnergyWindowsOverallAverage() {
		return fractionOfLowEnergyWindowsOverallAverage;
	}

	public double getZeroCrossingsOverallAverage() {
		return zeroCrossingsOverallAverage;
	}

	public double getStrongestBeatOverallAverage() {
		return strongestBeatOverallAverage;
	}

	public double getBeatSumOverallAverage() {
		return beatSumOverallAverage;
	}

	public double getStrengthOfStrongestBeatOverallAverage() {
		return strengthOfStrongestBeatOverallAverage;
	}

	public double[] getlPCOverallAverage() {
		return lPCOverallAverage;
	}

	public double[] getMethodofMomentsOverallAverage() {
		return methodofMomentsOverallAverage;
	}

	public final static int SPECTRAL_CENTROID_OVERALL_STANDARD_DEVIATION = 0,
			SPECTRAL_ROLLOFF_POINT_OVERALL_STANDARD_DEVIATION = 1, SPECTRAL_FLUX_OVERALL_STANDARD_DEVIATION = 2,
			COMPACTNESS_OVERALL_STANDARD_DEVIATION = 3, SPECTRAL_VARIABILITY_OVERALL_STANDARD_DEVIATION = 4,
			ROOT_MEAN_SQUARE_OVERALL_STANDARD_DEVIATION = 5,
			FRACTION_OF_LOW_ENERGY_WINDOWS_OVERALL_STANDARD_DEVIATION = 6,
			ZERO_CROSSINGS_OVERALL_STANDARD_DEVIATION = 7, STRONGEST_BEAT_OVERALL_STANDARD_DEVIATION = 8,
			BEAT_SUM_OVERALL_STANDARD_DEVIATION = 9, STRENGTH_OF_STRONGEST_BEAT_OVERALL_STANDARD_DEVIATION = 10,
			MFCC_OVERALL_STANDARD_DEVIATION = 11, LPC_OVERALL_STANDARD_DEVIATION = 12, METHOD_OF_MOMENTS_OVERALL_STANDARD_DEVIATION = 13,
			AREA_METHOD_OF_MOMENTS_OF_MFCCS_OVERALL_STANDARD_DEVIATION = 14, SPECTRAL_CENTROID_OVERALL_AVERAGE = 15,
			SPECTRAL_ROLL_OFF_POINT_OVERALL_AVERAGE = 16, SPECTRAL_FLUX_OVERALL_AVERAGE = 17,
			COMPACTNESS_OVERALL_AVERAGE = 18, SPECTRALVARIABILITYOVERALLAVERAGE = 19,
			ROOT_MEAN_SQUARE_OVERALL_AVERAGE = 20, FRACTION_OF_LOW_ENERGY_WINDOWS_OVERALL_AVERAGE = 21,
			ZERO_CROSSINGS_OVERALL_AVERAGE = 22, STRONGEST_BEAT_OVERALL_AVERAGE = 23, BEAT_SUM_OVERALL_AVERAGE = 24,
			STRENGTH_OF_STRONGEST_BEAT_OVERALL_AVERAGE = 25, LPC_OVERALL_AVERAGE = 26,
			METHOD_OF_MOMENTS_OVERALL_AVERAGE = 27, AREA_METHOD_OF_MOMENTS_OF_MFCCS_OVERALL_AVERAGE = 28, MFCC_Overall_Average = 29;
}
