package manipulaAudio;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.midi.MidiFileFormat;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
//import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import sun.audio.AudioData;
import sun.audio.AudioStream;


public class CarregaAudio {
    File arquivoDeAudio;
	AudioFormat formatoDoAudio;
	AudioInputStream audioInputStream;
	AudioInputStream ais;
	MidiFileFormat midiformat;
	AudioFileFormat audioFileFormat;

	public File getArquivoDeAudio() {
		return arquivoDeAudio;
	}

	public AudioFileFormat getAudioFileFormat() {
		return audioFileFormat;
	}

	public AudioFormat getFormatoDoAudio() {
		return formatoDoAudio;
	}

	public AudioInputStream getAudioInputStream() {
		return audioInputStream;
	}

	public AudioInputStream getAIS() {
		return ais;
	}

	public boolean carregaAudio(JFrame mainFrame) {
		boolean isErro = true;
        JFileChooser openDiag = new JFileChooser();

        
        if(JFileChooser.APPROVE_OPTION == openDiag.showOpenDialog(mainFrame)) {
            File selected = openDiag.getSelectedFile();
            
            try {
                
                /*
                 * first test to see if format supported.
                 * sometimes the system will claim to support a format
                 * but throw a LineUnavailableException on SourceDataLine.open
                 * 
                 * if retrieving a list of DataLine.Info for available
                 * supported formats some systems return -1 for sample rates
                 * indicating 'any' but evidently can be untrue: throws the exception.
                 * 
                 */
                
                AudioFileFormat fmt = AudioSystem.getAudioFileFormat(selected);
                arquivoDeAudio = selected;
                formatoDoAudio = fmt.getFormat();
        		audioInputStream = AudioSystem.getAudioInputStream(arquivoDeAudio);
        		audioFileFormat = AudioSystem.getAudioFileFormat(arquivoDeAudio);
        		isErro = false;
            } catch(IOException ioe) {
                showError(ioe);
            } catch(UnsupportedAudioFileException uafe) {
                showError(uafe);
            }
        }
        return isErro;
    }

    private void showError(Throwable t) {
        JOptionPane.showMessageDialog(null,
            "Exception <" + t.getClass().getName() + ">" +
            " with message '" + t.getMessage() + "'.",
            "Erro ao Carregar Arquivo",
            JOptionPane.WARNING_MESSAGE
        );
    }



}
