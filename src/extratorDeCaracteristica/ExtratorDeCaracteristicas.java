package extratorDeCaracteristica;

import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.swing.JOptionPane;

import gui.JanelaPrincipal;
import jAudioFeatureExtractor.Controller;
import jAudioFeatureExtractor.DataModel;
import jAudioFeatureExtractor.ExtractionThread;
import jAudioFeatureExtractor.ModelListener;
import jAudioFeatureExtractor.OuterFrame;
import jAudioFeatureExtractor.DataTypes.RecordingInfo;
import jAudioFeatureExtractor.jAudioTools.FP;
import jAudioFeatureExtractor.jAudioTools.FeatureProcessor;

public class ExtratorDeCaracteristicas {
	private static Controller controller  = new Controller();
	public static OuterFrame outer_frame = new OuterFrame(controller);
	static public FP fp;
	static boolean save_features_for_each_window = false;
	static boolean save_overall_recording_features = true;
	static OutputStream feature_values_save_path;
	static OutputStream feature_definitions_save_path;
	static int window_size = 512;
	static double window_overlap = 0.0;
	static boolean normalise = false;
	static double sampling_rate = 16000.0;
	static int outputType = 0;
	static RecordingInfo[] recording_info;

	public ExtratorDeCaracteristicas(int quantidadeDePadroes, String file) throws Exception {
		recording_info = new RecordingInfo[quantidadeDePadroes];
		feature_values_save_path =  new FileOutputStream("feature_values_1.xml");
		feature_definitions_save_path =  new FileOutputStream("feature_definitions_1.xml");

		

		//outer_frame = new OuterFrame(controller);
		for(int i=0;i<JanelaPrincipal.quantidadeDePadroes;i++){
			recording_info[i] = new RecordingInfo(file+" ("+i+").wav");
		}
		outer_frame.controller.dm_.recordingInfo = recording_info;
		ModelListener ml = new ModelListener() {
			
			@Override
			public void updateTable() {
				// TODO Auto-generated method stub
				
			}
		};
		DataModel dm = new DataModel("features.xml", ml);
		extractFeatures(controller);
		controller.extractionThread = new ExtractionThread(controller,outer_frame);

		controller.extractionThread.setup(save_overall_recording_features,save_features_for_each_window, "feature_values_1.xml",
				"feature_definitions_1.xml", window_size, window_overlap);
		controller.extractionThread.start();
    
	}
	
	private void extractFeatures(Controller controler) throws Exception {
		// Get the control parameters
		boolean save_features_for_each_window = true;
		boolean save_overall_recording_features = true;
		OutputStream feature_values_save_path =  new FileOutputStream("feature_values_1.xml");
		OutputStream feature_definitions_save_path =  new FileOutputStream("feature_definitions_1.xml");
		int window_size = 512;
		double window_overlap = 0.0;
		boolean normalise = false;//controller.normalise.isSelected();
		double sampling_rate = 16000.0;//controller.samplingRateAction.getSamplingRate();
		int outputType = 0;//controller.outputTypeAction.getSelected();
		try {

			// Ask user if s/he wishes to change window size to a power of 2 if
			// it
			// is not already.
			if (window_size >= 0) {
				int pow_2_size = jAudioFeatureExtractor.GeneralTools.Statistics.ensureIsPowerOfN(window_size, 2);
				if (window_size != pow_2_size) {
					String message = "Given window size is " + window_size
							+ ", which is not a power\n"
							+ "of 2. Would you like to increase this to the\n"
							+ "next highest power of 2 (" + pow_2_size + ")?";
					int convert = JOptionPane.showConfirmDialog(null, message,
							"WARNING", JOptionPane.YES_NO_OPTION);
				}
			}

			// Find which features are selected to be saved
			 boolean[] features_to_save = new  boolean[controller.dm_.features.length];
			 for (int i = 0; i < controller.dm_.defaults.length; i++) {
				 features_to_save[i] = ((Boolean) outer_frame.controller.fstm_.getValueAt(i,0)).booleanValue();
				controller.dm_.defaults[i] = features_to_save[i];
			}
//				controller.dm_.defaults[89] = true;
//				controller.dm_.featureDefinitions[89].dimensions = 20;


		} catch (Throwable t) {
			// React to the Java Runtime running out of memory
			if (t.toString().equals("java.lang.OutOfMemoryError"))
				JOptionPane
						.showMessageDialog(
								null,
								"The Java Runtime ran out of memory. Please rerun this program\n"
										+ "with a higher amount of memory assigned to the Java Runtime heap.",
								"ERROR", JOptionPane.ERROR_MESSAGE);
			else if (t instanceof Exception) {
				Exception e = (Exception) t;
				JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		fp = new FP(window_size, window_overlap, sampling_rate, normalise, controler.dm_.features,  controler.dm_.defaults, save_features_for_each_window, save_overall_recording_features, feature_values_save_path, feature_definitions_save_path, outputType);
	}
}
