package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import classificadores.Classificador;
import classificadores.ExtracaoAmostragem;
import extratorDeCaracteristica.ExtratorDeCaracteristicas;
import jAudioFeatureExtractor.jAudioTools.AudioSamples;
import manipulaArquivo.ArquivoJson;
import manipulaArquivo.ListaCaracteristicasObjeto;
import manipulaArquivo.ListaDeDadosDeMarcacao;
import manipulaAudio.CarregaAudio;
import objetos.Classificacao;
import sun.audio.AudioStream;

public class JanelaPrincipal {

	JanelaPrincipal jP;
	public File arquivoDeAudio;
	public AudioFormat formatoDoAudio;
	public AudioInputStream audioInputStream;
	public AudioFileFormat audioFileFormat;
	public static int quantidadeDePadroes = 344;	

	private JFrame mainFrame = new JFrame("Analisador Vocal");
	private JButton botaoCarregarAudio = new JButton("Carregar Audio");
	private JButton botaoExecutar = new JButton("Executar Classificação");
	private JButton botaoLerArquivoSepera = new JButton("Ler Arquivo / Separar");
	private JButton botaoExtratorDeCaracteristica = new JButton("Extrair Caracteristica");
	private JLabel labelStatus = new JLabel(":");
	private JPanel painelPrincipal = new JPanel(new BorderLayout());
	private JPanel painelBotoesInferior = new JPanel(new BorderLayout());
	private JPanel painelSeletorDeCaracteristicas = new JPanel(new BorderLayout());


	public JanelaPrincipal() {
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		botaoExecutar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ListaCaracteristicasObjeto listaCaracteristicasObjeto = ArquivoJson.lerJsonAggregatorContainer();
				ExtracaoAmostragem extracaoAmostragem = new ExtracaoAmostragem();
				for(int i =0;i<100;i++){
				extracaoAmostragem.extracaoRandomica(listaCaracteristicasObjeto.getListaCaracteristicasObjeto(), Classificador.EXTRACAO_KNN,i);
				}
			}
		});
		botaoExtratorDeCaracteristica.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					ExtratorDeCaracteristicas estrator = new ExtratorDeCaracteristicas(quantidadeDePadroes, "audioFelicidade/audio");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		botaoLerArquivoSepera.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0){
				ArquivoJson arquivoJson = new ArquivoJson();
				ListaDeDadosDeMarcacao listaDeDadosDeMarcacao = new ListaDeDadosDeMarcacao("marcacaoTeste3.txt");
				AudioSamples dadosDoAudio;
				try {
					dadosDoAudio = new AudioSamples(audioInputStream, arquivoDeAudio.getPath(), false);
					quantidadeDePadroes = listaDeDadosDeMarcacao.getListaDosDadosDeMarcacao().size();
					for (int i=0;i<listaDeDadosDeMarcacao.getListaDosDadosDeMarcacao().size();i++){				
						AudioSamples audioTemp = new AudioSamples(dadosDoAudio.getSamplesChannelSegregated(listaDeDadosDeMarcacao.getListaDosDadosDeMarcacao().get(i).getMarcacaoInicio(),
								listaDeDadosDeMarcacao.getListaDosDadosDeMarcacao().get(i).getMarcacaoFim()),
								formatoDoAudio, String.valueOf(i),false);
						File arquivo = new File("audio/audio"+i+"-"+listaDeDadosDeMarcacao.getListaDosDadosDeMarcacao().get(i).getClasse()+".wav");
						
						audioTemp.saveAudio(arquivo, true, audioFileFormat.getType(), false);
						System.out.println("Salvou "+(i));
					}
					System.out.println("Terminou");

//					arquivoJson.salvaJsonDadosDeMarcacao(listaDeDadosDeMarcacao);
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		});
		botaoCarregarAudio.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				CarregaAudio carregaAudio = new CarregaAudio();
				if (!carregaAudio.carregaAudio(mainFrame)) {
					labelStatus.setText("<html>" + carregaAudio.getArquivoDeAudio().getPath().toString() + "<br> Channels: "
							+ carregaAudio.getFormatoDoAudio().getChannels() + "<br> FrameRate: "
							+ carregaAudio.getFormatoDoAudio().getFrameRate() + "<br> FrameSize: "
							+ carregaAudio.getFormatoDoAudio().getFrameSize() + "<br> Encoding: "
							+ carregaAudio.getFormatoDoAudio().getEncoding() + "<br> FrameLength:"
							+ carregaAudio.getAudioInputStream().getFrameLength() + "</html>");
					arquivoDeAudio = carregaAudio.getArquivoDeAudio();
					formatoDoAudio = carregaAudio.getFormatoDoAudio();
					audioInputStream = carregaAudio.getAudioInputStream();
					audioFileFormat = carregaAudio.getAudioFileFormat();
				} else {
					System.out.println("Erro ao ler Arquivo");
				}

			}
		});

		painelPrincipal.add(botaoCarregarAudio, BorderLayout.NORTH);
		painelPrincipal.add(labelStatus, BorderLayout.CENTER);

		painelBotoesInferior.add(botaoLerArquivoSepera, BorderLayout.WEST);
		painelBotoesInferior.add(botaoExtratorDeCaracteristica, BorderLayout.CENTER);
		painelBotoesInferior.add(botaoExecutar, BorderLayout.EAST);
		painelPrincipal.add(painelBotoesInferior, BorderLayout.SOUTH);
		painelSeletorDeCaracteristicas.add(extratorDeCaracteristica.ExtratorDeCaracteristicas.outer_frame.feature_selector_panel);
		painelPrincipal.add(painelSeletorDeCaracteristicas, BorderLayout.WEST);
		mainFrame.setContentPane(painelPrincipal);
		mainFrame.pack();
		mainFrame.setSize(900, 600);
		mainFrame.setResizable(false);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}

	public static void main(String[] args) {

		JanelaPrincipal jP = new JanelaPrincipal();
	}
	
}
