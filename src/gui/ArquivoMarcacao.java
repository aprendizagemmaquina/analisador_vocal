package gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


import manipulaArquivo.GeracaoTxt;

public class ArquivoMarcacao {
	

	private JFrame mainFrame = new JFrame("Marcador");
	private JButton botaoIniciarMarcacao = new JButton("Iniciar Marca��o");
	private JButton botaoMarcar = new JButton("Marcar");
	private JPanel painelPrincipal = new JPanel(new BorderLayout());
	private int minuto= 0,segundo= 0, milisegundo=0;
	private String marcacaoInicio = "",marcacaoFim = "";
	private SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
	
	public ArquivoMarcacao(){
		
		mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		mainFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});
		botaoIniciarMarcacao.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				thread();		
			}
		});
		botaoMarcar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(marcacaoInicio.isEmpty()){
					
					marcacaoInicio = minuto+":"+segundo+":"+milisegundo;
				}else{
					marcacaoFim = minuto+":"+segundo+":"+milisegundo;
					GeracaoTxt.gravarTexto("marcacaoTeste.txt", ";"+marcacaoInicio+";"+marcacaoFim+";");
					marcacaoFim = "";
					marcacaoInicio = "";
					
				}
				
			}
		});
		
		painelPrincipal.add(botaoIniciarMarcacao, BorderLayout.WEST);
		painelPrincipal.add(botaoMarcar, BorderLayout.EAST);
		
		mainFrame.setContentPane(painelPrincipal);
		mainFrame.pack();
		mainFrame.setSize(200, 100);
		mainFrame.setResizable(false);
		mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
	}

	public static void main(String[] args) {
		ArquivoMarcacao jP = new ArquivoMarcacao();
			
	}
	
	private void thread(){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true){
					try {
						Thread.sleep(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if(milisegundo == 999){
						if(segundo == 59){
							segundo = 0;
							minuto++;
						}else{
							segundo++;
						}					
						milisegundo = 0;
					}else{
						milisegundo++;
					}
					System.out.println(minuto+":"+segundo+":"+milisegundo);
				}						
			}
		}).start();	
	}

}
