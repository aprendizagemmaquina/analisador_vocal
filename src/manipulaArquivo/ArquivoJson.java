package manipulaArquivo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import objetos.CaracteristicasObject;

public class ArquivoJson {
	static Gson gson = new Gson();

	public void salvaJsonDadosDeMarcacao(ListaDeDadosDeMarcacao listaDadosMarcacao) {
		// converte objetos Java para JSON e retorna JSON como String
		String json = gson.toJson(listaDadosMarcacao);

		try {
			// Escreve Json convertido em arquivo chamado "file.json"
			FileWriter writer = new FileWriter("dados_marcacao.json");
			writer.write(json);
			writer.close();
			System.out.println("Arquivo Salvo com Sucesso");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void salvaJsonAggregatorContainer(ListaCaracteristicasObjeto list) {
		// converte objetos Java para JSON e retorna JSON como String
		String json = gson.toJson(list);
		try {
			// Escreve Json convertido em arquivo chamado "file.json"
			FileWriter writer = new FileWriter("caracteristicasExtraidas.json");
			writer.write(json);
			writer.close();
			System.out.println("Arquivo Salvo com Sucesso");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static ListaCaracteristicasObjeto lerJsonAggregatorContainer() {
		ListaCaracteristicasObjeto listaCaracteristicasObjeto = null;
		Gson gson = new Gson();
		try {
			BufferedReader br = new BufferedReader(new FileReader("caracteristicasExtraidas.json"));
			// Converte String JSON para objeto Java
			listaCaracteristicasObjeto = gson.fromJson(br, ListaCaracteristicasObjeto.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listaCaracteristicasObjeto;
	}

	public void lerJsonDadosDeMarcacao() {
		Gson gson = new Gson();
		try {
			BufferedReader br = new BufferedReader(new FileReader("MFCC.txt"));
			// Converte String JSON para objeto Java
			ListaDeDadosDeMarcacao obj = gson.fromJson(br, ListaDeDadosDeMarcacao.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<CaracteristicasObject> lerJsonMfcc(String Path) {
		ArrayList<CaracteristicasObject> lista = new ArrayList<CaracteristicasObject>();
		try {
			BufferedReader br = new BufferedReader(new FileReader("MFCC.txt"));
			StringBuilder builder = new StringBuilder();
			String aux = "";
			while ((aux = br.readLine()) != null) {
				builder.append(aux);
			}
			String text = builder.toString();
			JsonArray jsonArray = (JsonArray) new JsonParser().parse(text);
			for (int i = 0; i < jsonArray.size(); i++) {
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				ArrayList<Double> listaD = new ArrayList();
				JsonArray jsonArray2 = jsonObject.getAsJsonArray("feature").get(0).getAsJsonObject().get("v").getAsJsonArray();
				for (int j = 0; j < jsonArray2.size(); j++) {
					listaD.add(jsonArray2.get(j).getAsDouble());
				}
				String nome = jsonObject.get("data_set_id").getAsString();
				String[] partes = nome.split("-");
				// lista.add(new CaracteristicasObject(partes[0], listaD,
				// partes[1]));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lista;
	}
}
