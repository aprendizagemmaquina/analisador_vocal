package manipulaArquivo;

import java.util.ArrayList;

public class ListaDeDadosDeMarcacao {
	private ArrayList<DadosDeMarcacao> listaDeDadosDeMarcacao;

	public ArrayList<DadosDeMarcacao> getListaDosDadosDeMarcacao() {
		return listaDeDadosDeMarcacao;
	}

	public ListaDeDadosDeMarcacao(String arquivoDoDeDadosDeMarcacao) {
		Arquivo arquivo = new Arquivo();
		arquivo.lerArquivoDeMarcacoes(arquivoDoDeDadosDeMarcacao);
		listaDeDadosDeMarcacao = arquivo.listaDeDadosDeMarcacao;
	}
	
	
}
