package manipulaArquivo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Arquivo {

	private ArrayList<String> conteudoDoArquivoDeMarcacao;
	
	public ArrayList<DadosDeMarcacao> listaDeDadosDeMarcacao = new ArrayList<DadosDeMarcacao>();

	public void lerArquivoDeMarcacoes(String nomeDoArquivo){
		conteudoDoArquivoDeMarcacao = new ArrayList<String>();    
		FileReader arquivo;
		int index = 0;
		try {
			arquivo = new FileReader(nomeDoArquivo);
			BufferedReader ler = new BufferedReader(arquivo);
				while(true){
					try {
						conteudoDoArquivoDeMarcacao.add(ler.readLine());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(conteudoDoArquivoDeMarcacao.get(index++)==null)
					break;
	     }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DadosDeMarcacao dadosDeMarcao;
		//------------
		for (int i = 0; i < conteudoDoArquivoDeMarcacao.size()-1; i++) {
			int contador = 0;
			String conteudo = "";
			int id = 0;
			double marcacaoInicio = 0;
			double marcacaoFim = 0;
			String classe = "";
			String pronuncia = "";

			for (int j = 0; j < conteudoDoArquivoDeMarcacao.get(i).length(); j++) {
				if (conteudoDoArquivoDeMarcacao.get(i).charAt(j) != ';' || conteudoDoArquivoDeMarcacao.get(i).charAt(j) == '\n') {
					conteudo = conteudo + conteudoDoArquivoDeMarcacao.get(i).charAt(j);
				} else {
					if (contador == 0) {
						id = Integer.parseInt(conteudo);
					}			
					if (contador == 1) {
						marcacaoInicio = Double.parseDouble(conteudo);
					}					
					if (contador == 2) {
						marcacaoFim = Double.parseDouble(conteudo);
					}					
					if (contador == 3) {
						classe = conteudo;
					}					
					if (contador == 4) {
						pronuncia = conteudo;
					}					
					contador++;
					conteudo = "";
				}
			}
			dadosDeMarcao = new DadosDeMarcacao(id, marcacaoInicio, marcacaoFim, classe, pronuncia);
			listaDeDadosDeMarcacao.add(dadosDeMarcao);
		}
	}
}
