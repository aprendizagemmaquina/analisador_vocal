package manipulaArquivo;

public class DadosDeMarcacao {
	private int id;
	private double marcacaoInicio;
	private double marcacaoFim;
	private String classe;
	private String pronuncia;
	private double[][] sample;
	private double[] coeficiente;
	
	public int getId() {
		return id;
	}
	public double getMarcacaoInicio() {
		return marcacaoInicio;
	}
	public double getMarcacaoFim() {
		return marcacaoFim;
	}
	public String getClasse() {
		return classe;
	}
	public String getPronuncia() {
		return pronuncia;
	}
	public double[][] getSample() {
		return sample;
	}
	public void setSample(double[][] sample) {
		this.sample = sample;
	}
	public double[] getCoeficiente() {
		return coeficiente;
	}
	public void setCoeficiente(double[] coeficiente) {
		this.coeficiente = coeficiente;
	}

	
	public DadosDeMarcacao(int id, double marcacaoInicio, double marcacaoFim, String classe, String pronuncia) {
		this.id = id;
		this.marcacaoInicio = marcacaoInicio;
		this.marcacaoFim = marcacaoFim;
		this.classe = classe;
		this.pronuncia = pronuncia;
	}
	
	public DadosDeMarcacao(int id, double marcacaoInicio, double marcacaoFim, String classe, String pronuncia,
			double[][] sample, double[] coeficiente) {
		this.id = id;
		this.marcacaoInicio = marcacaoInicio;
		this.marcacaoFim = marcacaoFim;
		this.classe = classe;
		this.pronuncia = pronuncia;
		this.sample = sample;
		this.coeficiente = coeficiente;
	} 
	
	
	
}
