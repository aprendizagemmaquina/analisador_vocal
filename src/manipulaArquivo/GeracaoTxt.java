package manipulaArquivo;

import java.io.FileWriter;
import java.io.IOException;

public class GeracaoTxt {
	
	public static void gravarTexto(String diretorio, String dado){

		try {
			FileWriter writer = new FileWriter(diretorio,true);
			writer.write(dado);
			writer.write("\n");
			writer.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
