package classificadores;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import manipulaArquivo.GeracaoTxt;
import objetos.CaracteristicasObject;

public class Classificador {
	public static final int EXTRACAO_KNN = 0;
	private final int TAMANHO_KNN_MAIORIA = 3;

	public void classificacaoKnn(List<CaracteristicasObject> teste, List<CaracteristicasObject> treino,
			int caracteristicaAnalizada) {

		int acerto = 0;
		int erros = 0;

		for (CaracteristicasObject linhaTeste : teste) {

			double[][] valor = new double[treino.size()][2];
			for (int i = 0; i < treino.size(); i++) {
				valor[i][0] = linhaTeste.compare(treino.get(i), caracteristicaAnalizada);
				valor[i][1] = i;
			}

			valor = bubbleSort(valor);

			// System.out.println("----------------------------------------------------------------------------------------------");
			// System.out.println(linhaTeste.getGrupo());
			// System.out.println("----------------------------------------------------------------------------------------------");
			// System.out.println("Distancia : "+valor[0][0] +"
			// "+treino.get((int) valor[0][1]).getGrupo());
			// System.out.println("Distancia : "+valor[1][0] +"
			// "+treino.get((int) valor[1][1]).getGrupo());
			// System.out.println("Distancia : "+valor[2][0] +"
			// "+treino.get((int) valor[2][1]).getGrupo());
			// System.out.println("Distancia : "+valor[3][0] +"
			// "+treino.get((int) valor[3][1]).getGrupo());
			// System.out.println("Distancia : "+valor[4][0] +"
			// "+treino.get((int) valor[4][1]).getGrupo());

			String[] grupoRecebidos = { treino.get((int) valor[0][1]).getGrupo(),
					treino.get((int) valor[1][1]).getGrupo(), treino.get((int) valor[2][1]).getGrupo(),
					treino.get((int) valor[3][1]).getGrupo(), treino.get((int) valor[4][1]).getGrupo() };

			if (verificarAcerto(linhaTeste.getGrupo(), grupoRecebidos)) {
				acerto++;
			} else {
				erros++;
			}

		}
		double taxaAcerto = ((double) acerto / (double) (acerto + erros));
		System.out.println("Taxa de acerto: " + taxaAcerto);
		GeracaoTxt.gravarTexto("Classificador " + nomeCaracteristica(caracteristicaAnalizada) + ".txt", String.valueOf(taxaAcerto)+"\t");
	}

	private boolean verificarAcerto(String grupoAvaliado, String[] gruposRecebidos) {

		int quantidadeAcertos = 0;

		for (int i = 0; i < gruposRecebidos.length; i++) {
			if (grupoAvaliado.equals(gruposRecebidos[i])) {
				quantidadeAcertos++;
			}
		}

		if (quantidadeAcertos >= TAMANHO_KNN_MAIORIA)
			return true;

		return false;
	}

	private double[][] bubbleSort(double[][] vetor) {
		boolean troca = true;
		double aux0, aux1;
		while (troca) {
			troca = false;
			for (int i = 0; i < vetor.length - 1; i++) {
				if (vetor[i][0] > vetor[i + 1][0]) {
					aux0 = vetor[i][0];
					aux1 = vetor[i][1];

					vetor[i][0] = vetor[i + 1][0];
					vetor[i][1] = vetor[i + 1][1];

					vetor[i + 1][0] = aux0;
					vetor[i + 1][1] = aux1;

					troca = true;
				}
			}
		}
		return vetor;
	}

	private String nomeCaracteristica(int identificador) {
		switch (identificador) {

		case CaracteristicasObject.SPECTRAL_CENTROID_OVERALL_STANDARD_DEVIATION:
			return "SPECTRAL_CENTROID_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.SPECTRAL_ROLLOFF_POINT_OVERALL_STANDARD_DEVIATION:
			return "SPECTRAL_ROLLOFF_POINT_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.SPECTRAL_FLUX_OVERALL_STANDARD_DEVIATION:
			return "SPECTRAL_FLUX_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.COMPACTNESS_OVERALL_STANDARD_DEVIATION:
			return "COMPACTNESS_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.SPECTRAL_VARIABILITY_OVERALL_STANDARD_DEVIATION:
			return "SPECTRAL_VARIABILITY_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.ROOT_MEAN_SQUARE_OVERALL_STANDARD_DEVIATION:
			return "ROOT_MEAN_SQUARE_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.FRACTION_OF_LOW_ENERGY_WINDOWS_OVERALL_STANDARD_DEVIATION:
			return "FRACTION_OF_LOW_ENERGY_WINDOWS_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.ZERO_CROSSINGS_OVERALL_STANDARD_DEVIATION:
			return "ZERO_CROSSINGS_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.STRONGEST_BEAT_OVERALL_STANDARD_DEVIATION:
			return "STRONGEST_BEAT_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.BEAT_SUM_OVERALL_STANDARD_DEVIATION:
			return "BEAT_SUM_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.STRENGTH_OF_STRONGEST_BEAT_OVERALL_STANDARD_DEVIATION:
			return "STRENGTH_OF_STRONGEST_BEAT_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.MFCC_OVERALL_STANDARD_DEVIATION:
			return "MFCC_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.LPC_OVERALL_STANDARD_DEVIATION:
			return "LPC_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.METHOD_OF_MOMENTS_OVERALL_STANDARD_DEVIATION:
			return "METHOD_OF_MOMENTS_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.AREA_METHOD_OF_MOMENTS_OF_MFCCS_OVERALL_STANDARD_DEVIATION:
			return "AREA_METHOD_OF_MOMENTS_OF_MFCCS_OVERALL_STANDARD_DEVIATION";
		case CaracteristicasObject.SPECTRAL_CENTROID_OVERALL_AVERAGE:
			return "SPECTRAL_CENTROID_OVERALL_AVERAGE";
		case CaracteristicasObject.SPECTRAL_ROLL_OFF_POINT_OVERALL_AVERAGE:
			return "SPECTRAL_ROLL_OFF_POINT_OVERALL_AVERAGE";
		case CaracteristicasObject.SPECTRAL_FLUX_OVERALL_AVERAGE:
			return "SPECTRAL_FLUX_OVERALL_AVERAGE";
		case CaracteristicasObject.COMPACTNESS_OVERALL_AVERAGE:
			return "COMPACTNESS_OVERALL_AVERAGE";
		case CaracteristicasObject.SPECTRALVARIABILITYOVERALLAVERAGE:
			return "SPECTRALVARIABILITYOVERALLAVERAGE";
		case CaracteristicasObject.ROOT_MEAN_SQUARE_OVERALL_AVERAGE:
			return "ROOT_MEAN_SQUARE_OVERALL_AVERAGE";
		case CaracteristicasObject.FRACTION_OF_LOW_ENERGY_WINDOWS_OVERALL_AVERAGE:
			return "FRACTION_OF_LOW_ENERGY_WINDOWS_OVERALL_AVERAGE";
		case CaracteristicasObject.ZERO_CROSSINGS_OVERALL_AVERAGE:
			return "ZERO_CROSSINGS_OVERALL_AVERAGE";
		case CaracteristicasObject.STRONGEST_BEAT_OVERALL_AVERAGE:
			return "STRONGEST_BEAT_OVERALL_AVERAGE";
		case CaracteristicasObject.BEAT_SUM_OVERALL_AVERAGE:
			return "BEAT_SUM_OVERALL_AVERAGE";
		case CaracteristicasObject.STRENGTH_OF_STRONGEST_BEAT_OVERALL_AVERAGE:
			return "STRENGTH_OF_STRONGEST_BEAT_OVERALL_AVERAGE";
		case CaracteristicasObject.LPC_OVERALL_AVERAGE:
			return "LPC_OVERALL_AVERAGE";
		case CaracteristicasObject.METHOD_OF_MOMENTS_OVERALL_AVERAGE:
			return "METHOD_OF_MOMENTS_OVERALL_AVERAGE";
		case CaracteristicasObject.AREA_METHOD_OF_MOMENTS_OF_MFCCS_OVERALL_AVERAGE:
			return "AREA_METHOD_OF_MOMENTS_OF_MFCCS_OVERALL_AVERAGE";
		case CaracteristicasObject.MFCC_Overall_Average:
			return "MFCC_Overall_Average";
		default:
			break;
		}
		return "";
	}

}
