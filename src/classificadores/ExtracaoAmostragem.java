package classificadores;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import objetos.CaracteristicasObject;

public class ExtracaoAmostragem {
	
	private final int TAMANHO = 60;
	
	public void extracaoRandomica(List<CaracteristicasObject> lista, int tipoClassificacao, int seed){
		Random gerador;
		if(seed != -1)
			gerador = new Random(seed);
		else
			gerador = new Random();
		
		ArrayList<CaracteristicasObject> avaliacao = new ArrayList();
		ArrayList<CaracteristicasObject> treino = new ArrayList();
		lista = removerDadosIrrelevantes(lista);
		int[] bloqueados = new int[TAMANHO];

		for (int j = 0; j < TAMANHO; j++) {
			
			int index;
			do{
				index = gerador.nextInt(lista.size());
			}while(contain(index, bloqueados));
			bloqueados[j] = index;
			avaliacao.add(lista.get(index));
		}

		for (int j = 0; j < lista.size(); j++) {
			boolean proibido = false;
			for (int j2 = 0; j2 < bloqueados.length; j2++) {
				if (j == bloqueados[j2]) {
					proibido = true;
				}
			}
			if (!proibido)
				treino.add(lista.get(j));
		}
		for(int i = 0;i<30;i++)
			executarClassificacao(avaliacao, treino, tipoClassificacao,i);
	}
	
	private boolean contain(int index, int[] bloqueados){
		for (int i = 0; i < bloqueados.length; i++) {
			if(index == bloqueados[i]){
				return true;
			}
				
		}
		return false;
	}
	
	public List<CaracteristicasObject> removerDadosIrrelevantes(List<CaracteristicasObject> lista){
	
		int i = 0;
		while (i < lista.size()) {
			if (lista.get(i).getAreaMethodofMomentsofMFCCsOverallAverage()[0] == 0) {
				lista.remove(i);
			} else
				i++;
		}
	
	return lista;
	}
	
	private void executarClassificacao(List<CaracteristicasObject> avaliacao, List<CaracteristicasObject> treino, int classificador, int caracteristicaAnalizada){
		switch (classificador) {
		case Classificador.EXTRACAO_KNN:
			new Classificador().classificacaoKnn(avaliacao, treino, caracteristicaAnalizada);
			break;

		default:
			break;
		}
	}

}